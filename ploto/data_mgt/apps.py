from django.apps import AppConfig


class DataMgtConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'data_mgt'
