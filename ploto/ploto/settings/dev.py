from common.common_crypte import decrypt_aes256gcm
from conf.conf import aes_gcm_key,mysql_pwd,mysql_aes_gcm_iv,mysql_aes_gcm_tag

ALLOWED_HOSTS = ["*"]
DEBUG = True
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "ploto_test",
        "USER": "ploto",
        "PASSWORD": decrypt_aes256gcm(mysql_pwd, aes_gcm_key, mysql_aes_gcm_iv, mysql_aes_gcm_tag),
        "HOST": "127.0.0.1",
        "PORT": "3306",
    },
    #'slave': {
    #    "ENGINE": "django.db.backends.mysql",
    #    #"NAME": "dashboard",
    #    "NAME": "dashboard_test",
    #    "USER": "root",
    #    "PASSWORD": settings_password,
    #    "HOST": "192.168.0.169",
    #    "PORT": "3306",
    #}
}