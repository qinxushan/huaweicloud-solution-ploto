import Vue from 'vue'
import App from './App.vue'
import i18n from './i18n'
import router from './router'
import auroraConfig from '../config/aurora'
import '@aurora/uem'
import iHelp from '@aurora/ihelp'

Vue.config.productionTip = false

/* AUI-INJECT-START-IHELP */
Vue.use(iHelp, auroraConfig.ihelp)
/* AUI-INJECT-END-IHELP */

new Vue({
  /* AUI-INJECT-START-VUE-OPTIONS */
  i18n: i18n({ locale: 'zh_CN' }),
  router,
  render: (h) => h(App)
  /* AUI-INJECT-END-VUE-OPTIONS */
}).$mount('#app')
