import Vue from 'vue'
import VueI18n from 'vue-i18n'
import locale from '@aurora/vue-locale'

Vue.use(VueI18n)

export default (i18n) =>
  locale.initI18n({
    i18n,
    VueI18n,
    messages: {
      zh_CN: {
        test: '中文'
      },
      en_US: {
        test: 'English'
      }
    }
  })
