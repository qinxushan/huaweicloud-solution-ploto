import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/views/homePage/HomePage.vue'
import LoginPage from '@/views/loginPage/LoginPage.vue'

Vue.use(Router)

// prettier-ignore
const routes = [

  /* AUI-INJECT-START-ROUTES */
  {
    path: '/',
    redirect: '/Login',
  },
  {
    path: '/index',
    name: '首页',
    component: HomePage
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  },
  {
    path: '/Login',
    name: '登录',
    component: LoginPage
  },
  
  /* AUI-INJECT-END-ROUTES */
]

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
