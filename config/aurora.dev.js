'use strict'
module.exports = {
  ihelp: {
    appId: 'Enter your iHelp app id',
    env: '//nkweb-uat.huawei.com'
  },
  uem: {
    env: 'beta',
    appKey: ''
  },

  contextPath: '/',
  haeStaticResUrl: '//r-beta.hw3static.com/s/haefrontend/lst/',
  appStaticResUrl: '',
  proxy: '',
  gateway: {
    // 公共服务配置信息
    'x-multi-consumer-appid': '',
    'x-multi-consumer-sub-appid': '',
    'x-multi-consumer-context': '',

    // 子业务配置信息
    'x-app-id': '',
    'x-sub-app-id': '',
    context: '',

    // 网关地址
    MsaGatewayAddress: ''
  }
}
