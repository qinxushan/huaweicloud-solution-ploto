'use strict'
module.exports = {
  ihelp: {
    appId: 'Enter your iHelp app id',
    env: '//app.huawei.com'
  },
  uem: {
    env: 'pro',
    appKey: ''
  },

  contextPath: '/',
  haeStaticResUrl: '//r.hw3static.com/s/haefrontend/lst/',
  appStaticResUrl: '',
  proxy: '',
  gateway: {
    // 子业务配置信息
    'x-app-id': '',
    'x-sub-app-id': '',
    context: '',

    // 公共服务配置信息
    'x-multi-consumer-appid': '',
    'x-multi-consumer-sub-appid': '',
    'x-multi-consumer-context': '',

    // 网关地址
    MsaGatewayAddress: ''
  }
}
