module.exports = {
  // baseUrl: '/',
  publicPath: require('./config/aurora').contextPath,
  outputDir: 'dist',
  assetsDir: 'assets',
  runtimeCompiler: true,
  productionSourceMap: true,
  parallel: true,
  css: {
    // 是否提取css 生产环境可以配置为 true
    extract: false
  },
  devServer: {
    proxy: false,
    host: 'localhost.huawei.com',
    open: true
  }
}
