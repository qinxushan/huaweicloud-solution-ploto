var express = require('express')
var compression = require('compression')
var contextPath = require('./vue.config').publicPath
var http = require('http')
var path = require('path')
var portrange = 8090
var app = express()

app.use(compression())
app.use(contextPath, express.static(path.resolve(__dirname, './dist/')))
app.use(
  contextPath,
  express.static(path.resolve(__dirname, './node_modules/@aurora/ui/web/'))
)
app.use(
  contextPath,
  express.static(path.resolve(__dirname, './node_modules/@aurora/service/web/'))
)
app.use(
  contextPath,
  express.static(
    path.resolve(__dirname, './node_modules/@aurora/service-hae/web/')
  )
)
app.use(
  contextPath,
  express.static(
    path.resolve(__dirname, './node_modules/@aurora/service-jalor5/web/')
  )
)
app.use(contextPath, express.static(path.resolve(__dirname, './web/')))

function getPort(cb) {
  var port = portrange
  portrange += 1

  var server = http.createServer()
  server.listen(port, function () {
    server.once('close', function () {
      cb(port)
    })
    server.close()
  })
  server.on('error', function () {
    getPort(cb)
  })
}

getPort(function (port) {
  app.listen(port, function () {
    require('child_process').exec(
      'start chrome http://localhost.huawei.com:' + port + contextPath
    )
  })
})
