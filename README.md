# huaweicloud-solution-ploto  
PLOTO：自动驾驶研发平台，集合业界优秀伙伴提供专业的自动驾驶研发工具。  
前端使用vue框架，后端采用Django框架。  
为自动驾驶全链路提供可靠服务（数据采集、数据传输、数据脱敏、场景数据提取、数据标注、AI训练、仿真测试、评估评价、OTA升级全链路流程）。  


## server端介绍
### conf文件配置
- **aes_gcm_key**  
    AES GCM加密算法的秘钥配置，秘钥长度32字节，填写密钥ascii码的base64格式字符串。可参考下方代码获取秘钥。
    建议周期性替换秘钥，修改密码。
    ```
    import binascii
    from Crypto import Random
    aes_gcm_key = binascii.b2a_base64(Random.get_random_bytes(32))
    ```

- **mysql配置**  
    配置后端服务mysql数据库的密码信息，填写base64格式的字符串。可参考下方代码获取对应的值。
    - mysql_pwd: 登录密码密文
    - mysql_aes_gcm_tag：密文校验信息
    - mysql_aes_gcm_iv： 加密随机数
    ```
    from conf.conf import aes_gcm_key
    mysql_pwd, mysql_aes_gcm_tag, mysql_aes_gcm_iv = encrypt_aes256gcm("密码铭文", key=aes_gcm_key, tag_len=16)
    ```
  
  
### 启动服务  
    本项目基于Django框架搭建后端服务，启动命令与Django保持一致  
    ```
    cd ploto
    python manage.py makemigrations # 需要迁移数据库时执行
    python manage.py migrate  # 需要迁移数据库时执行
    python manage.py runserver
    ```
    
## Web Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
